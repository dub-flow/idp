/* eslint-disable import/order */

// my app
const app = require('../../app');

// testing dependencies
const request = require('supertest')(app);
const should = require('chai').should();

// mocks
const { checkForUpdatesMock, downloadConfigMock, downloadFirmwareMock } = require('../mocks/updates.mock');

/**
  * Testing /updates/check endpoint
 */
describe('GET /api/updates/check', () => {
    let checkUpdateParams;

    // set checkUpdateParams to valid input before each request
    beforeEach(() => {
        // valid checkUpdateParams input
        checkUpdateParams = checkForUpdatesMock;
    });

    it('respond with OK 200', (done) => {
        request
            .get('/api/updates/check')
            .query(checkUpdateParams)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => res.body.should.have.property('message', '<SuccessfulUpdateCheck>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // no device_id
    it('respond with 422 because of missing device_id', (done) => {
        request
            .get('/api/updates/check')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)
            .expect(res => res.body.should.have.property('message', '<No device_id or not parseable to string>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });
});

/**
  * Testing /updates/firmware endpoint
 */
describe('GET /api/updates/firmware', () => {
    let downloadFirmwareParams;

    // set downloadFirmwareParams to valid input before each request
    beforeEach(() => {
        // valid downloadFirmwareParams input. Downloads a sample pdf (www.africau.edu/images/default/sample.pdf)
        downloadFirmwareParams = downloadFirmwareMock;
    });

    it('respond with OK 200', (done) => {
        request
            .get('/api/updates/firmware')
            .query(downloadFirmwareParams)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => res.body.should.have.property('message', '<SuccessfulFirmwareDownload>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // no device_id
    it('respond with 422 because of missing device_id', (done) => {
        request
            .get('/api/updates/firmware')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)
            .expect(res => res.body.should.have.property('message', '<No device_id or not parseable to string>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // new_firmware_url not parseable to Url
    it('respond with 422 because new_firmware_url is no Url', (done) => {
        // invalid Url
        downloadFirmwareParams.new_firmware_url = 'httT:www.test.de';

        request
            .get('/api/updates/firmware')
            .query(downloadFirmwareParams)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)
            .expect(res => res.body.should.have.property('message', '<No new_firmware_url or not parseable to Url>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // new_config_url is valid but non existing Url
    it('respond with 500 because new_firmware_url is valid Url but leads nowhere', (done) => {
        // invalid Url
        downloadFirmwareParams.new_firmware_url = 'www.ASDAAFHDKAHSDAKSDADASDHADKASDHASDKAHSDWQEOQWEQW.de';

        request
            .get('/api/updates/firmware')
            .query(downloadFirmwareParams)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(500)
            .expect(res => res.body.should.have.property('success', false))
            .expect(res => res.body.should.have.property('message', '<RequestError>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });
});


/**
  * Testing /updates/config endpoint
 */
describe('GET /api/updates/config', () => {
    let downloadConfigParams;

    // set downloadConfigParams to valid input before each request
    beforeEach(() => {
        // valid downloadConfigParams input. Downloads a sample pdf (www.africau.edu/images/default/sample.pdf)
        downloadConfigParams = downloadConfigMock;
    });

    it('respond with OK 200', (done) => {
        request
            .get('/api/updates/config')
            .query(downloadConfigParams)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => res.body.should.have.property('message', '<SuccessfulConfigDownload>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // no device_id
    it('respond with 422 because of missing device_id', (done) => {
        request
            .get('/api/updates/config')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)
            .expect(res => res.body.should.have.property('message', '<No device_id or not parseable to string>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // new_config_url not parseable to Url
    it('respond with 422 because new_config_url is no Url', (done) => {
        // invalid Url
        downloadConfigParams.new_config_url = 'httT:www.test.de';

        request
            .get('/api/updates/config')
            .query(downloadConfigParams)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)
            .expect(res => res.body.should.have.property('message', '<No new_config_url or not parseable to Url>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });


    // new_config_url is valid but non existing Url
    it('respond with 500 because new_config_url is valid Url but leads nowhere', (done) => {
        // non existing Url
        downloadConfigParams.new_config_url = 'www.ASDAAFHDKAHSDAKSDADASDHADKASDHASDKAHSDWQEOQWEQW.de';

        request
            .get('/api/updates/config')
            .query(downloadConfigParams)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(500)
            .expect(res => res.body.should.have.property('success', false))
            .expect(res => res.body.should.have.property('message', '<RequestError>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });
});
