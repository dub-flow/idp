// module dependencies
const express = require('express');
const { checkSchema } = require('express-validator/check');

// custom dependencies
const recfileLogic = require('../logic/recfile/recfileLogic');
const Val = require('../logic/Validations');
const Auth = require('../logic/Authentication');

// router
const router = express.Router();

// send recfile to the server. Therefore, a vehicle_device_token is needed for authentication
router.post('/', checkSchema(Val.validatePostRecfile()), Val.handleValidationErrors, Val.validateRecfiles,
    Val.validateRecfileWithChecksum, Auth.authenticate, recfileLogic.uploadRecfile);

module.exports = router;
