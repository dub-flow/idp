// module dependencies
const express = require('express');

// custom modules
const logger = require('./../../logger.js');

// import sub routers
const heartbeatRouter = require('./heartbeatRouter');
const updatesRouter = require('./updateRouter');
const recfileRouter = require('./recfileRouter');

// router
const router = express.Router();

// connect sub routers to the main router
router.use('/updates', updatesRouter);
router.use('/heartbeats', heartbeatRouter);
router.use('/recfiles', recfileRouter);

// hello world test route
router.get('/', (req, res) => res.send('Hello World'));

// CRLF route test
router.get('/test', (req, res) => {
    test = req.query.test;

    // prevent CRLF injection
    test = test.replace(/[\n\r]/g, "_");

    logger.info(`This is a test for CRLF injection: ${test}`);
    res.send('Tesssstt!')
})

module.exports = router;
