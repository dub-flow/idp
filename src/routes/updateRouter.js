// module dependencies
const express = require('express');
const { checkSchema } = require('express-validator/check');

// custom dependencies
const updateLogic = require('../logic/update/updateLogic');
const Val = require('../logic/Validations');

// router
const router = express.Router();

// check if either a firmware update or a config update exists (the actual update is not yet triggered)
router.get('/check', checkSchema(Val.validateCheckForUpdates()), Val.handleValidationErrors,
    Val.checkIfDeviceExists, updateLogic.checkForUpdates);

// download a firmware update (expects query params ?device_id=123&new_firmware_url=http://www.xy.de/test.exe)
router.get('/firmware', checkSchema(Val.validateDownloadFirmware()), Val.handleValidationErrors,
    Val.checkIfDeviceExists, updateLogic.downloadFirmwareUpdate);

// download a config update (expects query params ?device_id=123&new_config_url=http://www.xy.de)
router.get('/config', checkSchema(Val.validateDownloadConfig()), Val.handleValidationErrors,
    Val.checkIfDeviceExists, updateLogic.downloadConfigUpdate);

module.exports = router;
