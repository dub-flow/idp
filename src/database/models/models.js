// custom dependencies
const relations = require('./relations');

/**
 * Initializing the models.
 *
 * @param {Sequelize} sequelize the sequelize instance
 */
exports.init = (sequelize) => {
    // device schema
    sequelize.import('./definition/device/obd_firmware_version.js');
    sequelize.import('./definition/device/device.js');
    sequelize.import('./definition/device/obd_config_update.js');
    sequelize.import('./definition/device/obd_firmware_update.js');
    sequelize.import('./definition/device/obd_wifi_log.js');
    sequelize.import('./definition/device/recfile_upload_log.js');
    sequelize.import('./definition/device/user_vehicle_device.js');

    // vehicle schema
    sequelize.import('./definition/vehicle/user_vehicle.js');

    // trigger the initiation of relations
    relations.init(sequelize);
};
