// custom dependencies
const dbSchemata = require('../../../../../config').database.params.schema;

// the schema the table belongs to
const tableSchema = dbSchemata.vehicle;

/**
 * @typedef {Object} user_vehicle
 *
 * @property {number} id the ID of the user vehicle
 * @property {number} user_id the user the vehicle belongs to
 * @property {string} deleted_by_user flag to identify deleted users
 *
 * @return {user_vehicle}
 */
module.exports = (sequelize, DataTypes) => sequelize.define('user_vehicle', {
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    user_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        // references: {
        //     model: 'user',
        //     key: 'id',
        // },
        //
        // references user --> Since the user entity has not been implemented as model here, user_id is currently no foreign key
    },
    deleted_by_user: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
}, {
    schema: tableSchema,
    tableName: 'user_vehicle',
    // if timestamps are on true, sequelize is looking for columns named 'createdAt' and 'updatedAt'
    timestamps: false,
    // tells sequelize that table names are not camelcase but snakecase
    underscored: true,
});
