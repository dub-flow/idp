// custom dependencies
const dbSchemata = require('../../../../../config').database.params.schema;

// the schema the table belongs to
const tableSchema = dbSchemata.device;

// a schema this table references as foreign key
const foreignKeySchema = dbSchemata.vehicle;

/**
 * @typedef {Object} user_vehicle_device
 *
 * @property {number} id the ID of the user_vehicle_device
 * @property {number} user_vehicle_id the user_vehicle of the particular device
 * @property {string} device_id the device ID of the user_vehicle
 * @property {string} token the security token for authentication purposes
 * @property {Date} start_time the start time of the user_vehicle_device
 * @property {Date} stop_time the stop time of the user_vehicle_device
 * @property {boolean} is_active flag to determine if the user_vehicle_device is active
 *
 * @return {user_vehicle_device}
 */
module.exports = (sequelize, DataTypes) => sequelize.define('user_vehicle_device', {
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    user_vehicle_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        // references: {
        //     // model: 'user_vehicle',
        //     // references a key from a different schema
        //     model: {
        //         schema: foreignKeySchema,
        //         tableName: 'user_vehicle',
        //     },
        //     key: 'id',
        // },
    },
    device_id: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
            model: 'device',
            key: 'id',
        },
    },
    token: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    start_time: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    stop_time: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: true,
    },
}, {
    schema: tableSchema,
    tableName: 'user_vehicle_device',
    // if timestamps are on true, sequelize is looking for columns named 'createdAt' and 'updatedAt'
    timestamps: false,
    // tells sequelize that table names are not camelcase but snakecase
    underscored: true,
});
