// custom dependencies
const dbSchemata = require('../../../../../config').database.params.schema;

// the schema the table belongs to
const tableSchema = dbSchemata.device;

/**
 * @typedef {Object} obd_wifi_log
 *
 * @property {string} device_id the ID of the device
 * @property {Date} time the date the WIFI log entry has been created
 * @property {string} ssid_current_wifi thee SSID of the WIFI the entry has been created from
 * @property {number} lat_current_wifi_con the lat of the current WIFI connection
 * @property {number} long_current_wifi_con how long was the current WIFI connection
 * @property {string} ssid_previous_wifi the SSID of the previous WIFI
 * @property {Date} time_previous_wifi_lost the date the previous connection was lost
 * @property {number} rssi_previous_wifi_lost the RSSI of the previous WIFI to which the connection is already lost
 * @property {number} lat_previous_wifi_lost the lat of the previous WIFI connection
 * @property {number} long_previous_wifi_lost the amout of time the previous connection has been lost
 * @property {number} max_rssi the maximum RSSI of the connection
 * @property {Date} time_max_rssi the time of the maximum RSSI
 * @property {number} lat_max_rssi the lat of the maximum RSSI
 * @property {number} long_max_rssi how long the maximum RSSI was
 * @property {number} free_disk_space the free disk space of the device to write the WIFI log entry
 * @property {number} number_of_local_files the amount of local files on the device
 *
 * @return {obd_wifi_log}
 */
module.exports = (sequelize, DataTypes) => sequelize.define('obd_wifi_log', {
    device_id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
        // references: {
        //     model: 'device',
        //     key: 'id',
        // },
    },
    time: {
        type: DataTypes.DATE,
        allowNull: false,
        primaryKey: true,
    },
    ssid_current_wifi: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    lat_current_wifi_con: {
        // numeric (8, 6)
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    long_current_wifi_con: {
        // numeric (9, 6)
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    ssid_previous_wifi: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    time_previous_wifi_lost: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    rssi_previous_wifi_lost: {
        // numeric (6, 4)
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    lat_previous_wifi_lost: {
        // numeric (8, 6)
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    long_previous_wifi_lost: {
        // numeric (9, 6)
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    max_rssi: {
        // numeric (6, 4)
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    time_max_rssi: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    lat_max_rssi: {
        // numeric (8, 6)
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    long_max_rssi: {
        // numeric (9, 6)
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    free_disk_space: {
        // numeric (8, 2)
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    number_of_local_files: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
}, {
    schema: tableSchema,
    tableName: 'obd_wifi_log',
    // if timestamps are on true, sequelize is looking for columns named 'createdAt' and 'updatedAt'
    timestamps: false,
    // tells sequelize that table names are not camelcase but snakecase
    underscored: true,
});
