// custom dependencies
const dbSchemata = require('../../../../../config').database.params.schema;

// the schema the table belongs to
const tableSchema = dbSchemata.device;

/**
 * @typedef {Object} recfile_upload_log
 *
 * @property {number} id the ID of the log entry
 * @property {number} user_vehicle_id the ID of the user_vehicle the entry belongs to
 * @property {string} device_id the device used for the recfile upload
 * @property {Date} log_time the time the log entry has been created
 * @property {string} recfile_name the name of the recfile being uploaded
 * @property {string} source the source of the recfile
 *
 * @return {recfile_upload_log}
 */
module.exports = (sequelize, DataTypes) => sequelize.define('recfile_upload_log', {
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    user_vehicle_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    device_id: {
        type: DataTypes.STRING,
        allowNull: true,
        // references: {
        //     model: 'device',
        //     key: 'id',
        // },
    },
    log_time: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    recfile_name: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    source: {
        type: DataTypes.STRING,
        allowNull: true,
    },
}, {
    schema: tableSchema,
    tableName: 'recfile_upload_log',
    // if timestamps are on true, sequelize is looking for columns named 'createdAt' and 'updatedAt'
    timestamps: false,
    // tells sequelize that table names are not camelcase but snakecase
    underscored: true,
});
