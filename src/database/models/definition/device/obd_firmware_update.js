// custom dependencies
const dbSchemata = require('../../../../../config').database.params.schema;

// the schema the table belongs to
const tableSchema = dbSchemata.device;

/**
 * @typedef {Object} obd_firmware_update
 *
 * @property {string} device_id the ID of the device
 * @property {number} new_obd_firmware_version_id the id of the new firmware version
 * @property {Date} update_start_time the name of the firmware file
 *
 * @return {obd_firmware_update}
 */
module.exports = (sequelize, DataTypes) => sequelize.define('obd_firmware_update', {
    device_id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
        references: {
            model: 'device',
            key: 'id',
        },
    },
    new_obd_firmware_version_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'obd_firmware_version',
            key: 'id',
        },
    },
    update_start_time: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    schema: tableSchema,
    tableName: 'obd_firmware_update',
    // if timestamps are on true, sequelize is looking for columns named 'createdAt' and 'updatedAt'
    timestamps: false,
    // tells sequelize that table names are not camelcase but snakecase
    underscored: true,
});
