// custom dependencies
const dbSchemata = require('../../../../../config').database.params.schema;

// the schema the table belongs to
const tableSchema = dbSchemata.device;

/**
 * @typedef {Object} obd_config_update
 *
 * @property {string} device_id the ID of the device
 * @property {boolean} update_needed flag to determine if the device needs an update
 * @property {Date} update_start_time the time the update has started
 * @property {string} url the url to the config file to be used for the update
 * @property {string} check_sum the check sum of the config file
 *
 * @return {obd_config_update}
 */
module.exports = (sequelize, DataTypes) => sequelize.define('obd_config_update', {
    device_id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
        references: {
            model: 'device',
            key: 'id',
        },
    },
    update_needed: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false,
    },
    update_start_time: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    check_sum: {
        type: DataTypes.STRING,
        allowNull: true,
    },
}, {
    schema: tableSchema,
    tableName: 'obd_config_update',
    // if timestamps are on true, sequelize is looking for columns named 'createdAt' and 'updatedAt'
    timestamps: false,
    // tells sequelize that table names are not camelcase but snakecase
    underscored: true,
});
