/**
 * Initializing the relations between the models.
 *
 * @param {Sequelize} sequelize the sequelize instance
 */
exports.init = (sequelize) => {
    // the sequelize models (database tables)
    const { models } = sequelize;

    // device schema
    const { device } = models;
    const obdConfigUpdate = models.obd_config_update;
    const obdFirmwareUpdate = models.obd_firmware_update;
    const obdFirmwareVersion = models.obd_firmware_version;
    const obdWifiLog = models.obd_wifi_log;
    const recfileUploadLog = models.recfile_upload_log;
    const userVehicleDevice = models.user_vehicle_device;

    // vehicle schema
    // NOTE: userVehicle has a connection to the user table which is currently not part of the models
    const userVehicle = models.user_vehicle;

    // cascade options (to enforce the observance of key constraints)
    const cascade = {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    };

    /**
     * The associations between the models.
     *
     * @see http://docs.sequelizejs.com/manual/tutorial/associations.html
     * @example Player.belongsTo(Team); // Will add a teamId attribute to Player to hold the primary key value for Team
     */

    /** device - obdFirmwareVersion (FK) */
    // since the foreign key name of device 'current_obd_firmware_version_id' differs the Sequelize primary key name of obdFirmwareVersion,
    // which by Sequelize is expected to be 'current_obd_firmware_version_id' (or just 'id' if the table is called 'current_obd_firmware_version'),
    // an alias has to be provided.
    // Also possible would be precisely defining the foreign key by { foreignKey: 'current_obd_firmware_version_id', ...cascade }
    // instead of using both the aliases.
    device.belongsTo(obdFirmwareVersion, { as: 'current_obd_firmware_version', ...cascade });
    obdFirmwareVersion.hasOne(device, { as: 'current_obd_firmware_version', ...cascade });

    /** obdConfigUpdate - device (FK) */
    // no aliases needed here, since Sequelize expects the id of device to be referenced as device_id in foreign tables
    obdConfigUpdate.belongsTo(device, cascade);
    device.hasOne(obdConfigUpdate, cascade);

    /** obdFirmwareUpdate - device (FK) */
    // no aliases needed
    obdFirmwareUpdate.belongsTo(device, cascade);
    device.hasOne(obdFirmwareUpdate, cascade);

    /** obdFirmwareUpdate - obdFirmwareVersion (FK) */
    // since Sequelize would try expect the foreign key 'new_obd_firmware_version_id' to refer to a table
    // 'new_obd_firmware_version'. Since the table actually is called 'obd_firmware_version', an alias has
    // to be provided
    obdFirmwareUpdate.belongsTo(obdFirmwareVersion, { as: 'new_obd_firmware_version', ...cascade });
    obdFirmwareVersion.hasOne(obdFirmwareUpdate, { as: 'new_obd_firmware_version', ...cascade });

    /** obdWifiLog - device (FK) */
    // no aliases needed
    obdWifiLog.belongsTo(device, cascade);
    device.hasMany(obdWifiLog, cascade);

    /** recfileUploadLog - device (FK) */
    // no aliases needed
    recfileUploadLog.belongsTo(device, cascade);
    device.hasMany(recfileUploadLog, cascade);

    /** userVehicleDevice - device (FK) */
    // no aliases needed
    userVehicleDevice.belongsTo(device, cascade);
    device.hasOne(userVehicleDevice, cascade);

    /** userVehicleDevice - userVehicle (FK) */
    // no aliases needed
    userVehicleDevice.belongsTo(userVehicle, cascade);
    userVehicle.hasOne(userVehicleDevice, cascade);
};
