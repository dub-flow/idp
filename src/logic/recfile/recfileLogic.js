// module dependencies
const _ = require('lodash');
const fs = require('fs');
const uuidv1 = require('uuid/v1');

// custom dependencies
const DbHandler = require('../DbHandler');
const Responder = require('./../Responder');
const logger = require('./../../../logger');
const config = require('./../../../config');

/**
 * Upload a recfile. The recfiles are put into the recfile queue where they are
 * handled by an external application.
 *
 * @param {object} req
 * @param {object} res
 */
function uploadRecfile(req, res) {
    logger.verbose('Upload recfile functionality triggered');
    const responder = new Responder();
    const dbHandler = new DbHandler();

    // extract params
    const deviceId = req.body.device_id;
    const userVehicleId = req.body.user_vehicle_id;
    const recFiles = req.files;
    const { source } = req.body;

    // if there are neither too many nor not a single recfile to be handled
    logger.info(`${_.size(recFiles)} recfiles are provided for device ${deviceId}`);

    // the current time as UTC string
    const currentTime = new Date().toUTCString();

    // an aray of recfileLog entries
    const newRecfileLogEntries = [];

    // create a recfileLog entry for each recfile
    _.forOwn(recFiles, (file) => {
        newRecfileLogEntries.push({
            user_vehicle_id: userVehicleId,
            device_id: deviceId,
            log_time: currentTime,
            recfile_name: file.name,
            source,
        });
    });

    // write all recfileLog entries into the database
    logger.info(`Try writing into recfile_upload_log for device ${deviceId}`);
    dbHandler.bulkCreate('recfile_upload_log', newRecfileLogEntries)
        .then(() => {
            logger.info(`Writing into recfile_upload_log was successful for device ${deviceId}`);

            // make recfile name unique to avoid errors regarding multiple file with the same name
            makeFilenamesUnique(recFiles);
        })
        .then(() => new Promise((resolve, reject) => {
            // move all recfiles to the recfile queue
            const recfileErrors = moveRecfilesToQueue(recFiles, deviceId);

            // resolve all possible recfile errors. Since these errors might only affect one out of
            // several recfiles, the request is non totally rejected but partly handled here.
            resolve(recfileErrors);
        }))
        .then(data => responder.send(res, true, '<SuccessfulRecfileUpload>', null, data))
        .catch(error => responder.falseAndSend(res, error, 500));
}

exports.uploadRecfile = uploadRecfile;

// ######## HELPER FUNCTIONS ########
/**
 * Moves all recfiles to the recfile queue.
 *
 * @param {object} recFiles all recfiles to be moved
 * @param {string} deviceId the Id of the device that uploaded the recfiles
 */
function moveRecfilesToQueue(recFiles, deviceId) {
    const recfileErrors = [];

    // move recfiles to recfile queue
    _.forOwn(recFiles, (file) => {
        // create the recfile in the recfile queue folder
        const wstream = fs.createWriteStream(`${config.recfileQueueFolder}/${file.name}`);

        // when the writing is finished
        wstream.on('finish', () => {
            logger.info(`Recfile '${file.name}' for device ${deviceId} has been written`);
        });

        // error handler
        wstream.on('error', (error) => {
            logger.warn(`An error occurred writing recfile '${file.name}' for device ${deviceId}: ${error}`);
            recfileErrors.push(error);
        });

        // write recfile buffer to file
        wstream.write(file.data);
        wstream.end();
    });

    // return all recfileErrors
    return recfileErrors;
}

/**
 * Add a unique identifier to the name of each recfile. Add the uuid before
 * the file name and before the file extension like:
 * --> recfilenameXYZ (AFSDGFADFDGTJAHF).rec
 *
 * @param {object} recfiles all recfiles which names shall be make uniquely
 */
function makeFilenamesUnique(recfiles) {
    // iterate over all recfiles
    _.forOwn(recfiles, (file) => {
        // add the uuid before the file name and before the file extension like:
        // -- >recfilenameXYZ (UUID).rec
        file.name = `${(file.name).substring(0, (file.name).lastIndexOf('.'))} (${uuidv1()})${
            (file.name).substring((file.name).lastIndexOf('.'))}`;
    });
}
