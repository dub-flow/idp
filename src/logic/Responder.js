// custom dependencies
const logger = require('./../../logger');

/**
 * Module to centrally handle all server responses.
 */
module.exports = class Responder {
    constructor() {
        return {
            send: this.send,
            falseAndSend: this.falseAndSend,
        };
    }

    /**
     * Response the result to the client.
     *
     * @param {object} response the response object
     * @param {boolean} success flag to determine if the request was succesful
     * @param {string} msgCode a static message code
     * @param {string} error an error string (if something went wrong)
     * @param {object} data the actual requested data
     * @param {number} httpStatus the HTTP status code of a failure (200 on default)
     */
    // eslint-disable-next-line class-methods-use-this
    send(response, success, msgCode, error, data, httpStatus = 200) {
        const result = {
            success,
            message: msgCode,
            data,
            status: httpStatus,
        };

        // if an error occurred during the request
        if (success === false && error != null) {
            logger.error(error);
        }

        // send response to client
        response.status(httpStatus).send(result);
    }

    /**
     * Prefunction to handle a response for a failed request.
     *
     * @param {object} response the response object
     * @param {object} errorInfo contains info about what went wrong
     * @param {number} httpStatus the HTTP status code of the failure
     */
    falseAndSend(response, errorInfo, httpStatus = 500) {
        // if no error code is provided, the errorInfo object is fully logged here
        if (!errorInfo.msgCode) {
            logger.error(errorInfo);

            // add a msgCode based on the 'name' of the error
            errorInfo.msgCode = errorInfo.name ? `<${errorInfo.name}>` : '<Error (see log file)>';
        }

        // trigger the send function
        this.send(response, false, errorInfo.msgCode, errorInfo.err, {}, httpStatus);
    }
};
