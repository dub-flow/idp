// custom dependencies
const Responder = require('./../logic/Responder');
const DbHandler = require('./DbHandler');

/**
 * Module to centrally handle all token authentication.
 */
module.exports = class Authentication {
    /**
     * Try authenticating a device by usage of the vehicle_device_token.
     *
     * @param {object} req
     * @param {object} res
     * @param {object} next
     */
    static authenticate(req, res, next) {
        const dbhandler = new DbHandler();
        const responder = new Responder();

        // extract device_id
        const deviceId = req.body.device_id;
        // extract token from header
        const token = req.header('vehicle_device_token');

        // the query condition
        const condition = {
            where: { device_id: deviceId, token },
        };

        // check if the token exists for the provided deviceId
        dbhandler.findOne('user_vehicle_device', condition)
            .then((result) => {
                // check if there is no result for combination of provided deviceId and token
                if (!result) {
                    // create custom error
                    const error = {
                        msgCode: '<AuthenticationFailed>',
                        err: `Failed authentication approach for device ${deviceId}`,
                    };

                    return responder.falseAndSend(res, error, 401);
                }

                // if authentication was successful
                return next();
            })
            .catch(error => responder.falseAndSend(res, error, 500));
    }
};
