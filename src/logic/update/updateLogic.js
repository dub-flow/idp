// module dependencies
const url = require('url');
const request = require('request-promise');

// custom dependencies
const DbHandler = require('../DbHandler');
const Responder = require('./../Responder');
const logger = require('./../../../logger');
const config = require('./../../../config');

/**
 * Check for firmware and config updates for a provided device.
 *
 * Important: Since POST /heartbeats is always the first route to be triggered,
 * the provided device has to already exist in the device table.
 *
 * 1) Firstly, it is checked if the provided device needs a config update.
 *
 * 1a) If so, return a boolean determining that as well as the download Url
 *  and the checksum (not stored yet?).
 *
 * 1b) If not, simply return a false flag for the config update.
 *
 * 2) Next, it is checked if the provided device needs a firmware update.
 *
 * 2.1) First, check if that device already has an entry in obd_firmware_update.
 * 2.1a) If an entry already exist, simply persume with 2.2)
 * 2.1b) If no entry exists yet, create a default entry that leads to a firmware update to the default firmware
 *
 * 2.2) Second, get the actual firmware version of that device out of the 'device' table.
 *
 * 2.3) Third, check if that firmware version is smaller than the newest firmware version
        of table obd_firmware_update (column new_obd_firmware_version_id).
 *
 * 2.4a) If the device needs a firmware update, return a boolean determining that as well as the download Url
 *       and the checksum.
 *
 * 2.4b) If not, simply return a false flag for the firmware update.
 *
 * @param {object} req
 * @param {object} res
 */
async function checkForUpdates(req, res) {
    logger.verbose('Check for updates functionality triggered');
    const responder = new Responder();
    const reqDeviceId = req.query.device_id;

    // 1) Firstly, it is checked if the provided device needs a config update
    logger.info(`Check for updates for provided device_id: ${reqDeviceId}`);

    try {
        // asynchronously check for config and firmware updates
        const configUpdate = await checkForConfigUpdate(reqDeviceId);
        const firmwareUpdate = await checkForFirmwareUpdate(reqDeviceId);

        // send final response
        responder.send(res, true, '<SuccessfulUpdateCheck>', null, { config: configUpdate, firmware: firmwareUpdate });
    } catch (error) {
        responder.falseAndSend(res, error, 500);
    }

    // // handle all promises
    // Promise.all([checkForConfigUpdate(reqDeviceId), checkForFirmwareUpdate(reqDeviceId)])
    //     .then(updates => new Promise((resolve, reject) => {
    //         logger.info(`Update-check for device ${reqDeviceId} finished`);
    //         // map the config and firmware update to a json
    //         resolve({ config: updates[0], firmware: updates[1] });
    //     }))
    //     .then(data => responder.send(res, true, '<SuccessfulUpdateCheck>', null, data))
    //     .catch(error => responder.falseAndSend(res, error, 500));
}

exports.checkForUpdates = checkForUpdates;

/**
 * Download a config update.
 *
 * Important: At this point, the device actually exists (because of POST /heartbeats)
 * and entries already exists in the tables 'obd_config_update' and 'obd_firmware_update'
 * due to the latest check for updates.
 *
 * 1) Download the file at the provided url.
 *
 * 2) On download success, update the database to represent the latest config state.
 *
 * 3) Respond file to the client.
 *
 * @param {object} req
 * @param {object} res
 */
function downloadConfigUpdate(req, res) {
    logger.verbose('Download config update functionality triggered');
    const dbHandler = new DbHandler();
    const responder = new Responder();

    // extract url params
    const deviceId = req.query.device_id;
    let newConfigUrl = req.query.new_config_url;
    // const newConfigFile = req.query.new_config_filename;
    logger.info(`Init config update for device ${deviceId}, config url '${newConfigUrl}'`);

    // if no protocol is provided in the newConfigUrl (e.g. www.test.de), 'http' will be added
    if (url.parse(newConfigUrl).protocol === null) {
        newConfigUrl = `http://${newConfigUrl}`;
    }

    // 1) Download the file at the provided url
    request(newConfigUrl)
        .then(async (file) => {
            logger.info('Config file was succesfully downloaded');

            // 2) On download success, update the database to represent the latest config state.
            logger.info(`Start updating info related to the new config for device ${deviceId}`);

            // 2.1) Update update_start_time to current time in 'obd_config_update' and update_needed to false
            // the current time as UTC string
            const currentTime = new Date().toUTCString();

            // update the devices' entry in the obd_config_update table
            const updatedConfigUpdate = {
                update_needed: false,
                update_start_time: currentTime,
            };
            await dbHandler.update('obd_config_update', { where: { device_id: deviceId } }, updatedConfigUpdate);
            logger.info(`Table 'obd_config_update' updated for device ${deviceId}`);

            // 2.2) Update last_config_update to current time and current_obd_config_filename in 'device'
            const updatedDevice = {
                last_config_update: currentTime,
                current_obd_config_filename: '????', // TODO: current_obd_config_filename in 'device'?
            };
            await dbHandler.update('device', { where: { id: deviceId } }, updatedDevice);
            logger.info(`Table 'device' updated for device ${deviceId}`);

            // 3) Respond file to the client
            responder.send(res, true, '<SuccessfulConfigDownload>', null, file); // send the whole response object to the client
            // res.send(file); // would only send the file to the client
            // res.end(file); // makes e. g. a pdf actually viewable in a browser
        })
        .catch(error => responder.falseAndSend(res, error, 500));
}

exports.downloadConfigUpdate = downloadConfigUpdate;

/**
 * Download a firmware update.
 *
 * Important: At this point, the device actually exists (because of POST /heartbeats)
 * and entries already exists in the tables 'obd_config_update' and 'obd_firmware_update'
 * due to the latest check for updates.
 *
 * 1) Download the file at the provided url and filename.
 *
 * 2) On download success, update the database to represent the latest firmware state.
 *
 * 3) Respond file to the client.
 *
 * @param {object} req
 * @param {object} res
 */
function downloadFirmwareUpdate(req, res) {
    logger.verbose('Download firmware update functionality triggered');
    const dbHandler = new DbHandler();
    const responder = new Responder();

    // extract url params
    const deviceId = req.query.device_id;
    let newFirmwareUrl = req.query.new_firmware_url;
    logger.info(`Init firmware update for device ${deviceId}, firmware url '${newFirmwareUrl}'`);

    // if no protocol is provided in the newFirmwareUrl (e.g. www.test.de), 'http' will be added
    if (url.parse(newFirmwareUrl).protocol === null) {
        newFirmwareUrl = `http://${newFirmwareUrl}`;
    }

    // 1) Download the file to the provided url and filename
    request(newFirmwareUrl)
        .then(async (file) => {
            logger.info('Firmware file was succesfully downloaded');

            // 2) On download success, update the database to represent the latest firmware state.
            logger.info(`Start updating info related to the new firmware for device ${deviceId}`);

            // get the new firmware version of that device out of the table 'obd_firmware_update'
            const deviceFirmware = await dbHandler.findOne('obd_firmware_update', { where: { device_id: deviceId } });

            // the current time as UTC string
            const currentTime = new Date().toUTCString();

            // update last_firmware_update in 'device' to current time.
            // update current_obd_firmware_version_id in 'device' to the new_obd_firmware_version_id
            // of that device 'obd_firmware_update'.
            const updatedDevice = {
                last_firmware_update: currentTime,
                current_obd_firmware_version_id: deviceFirmware.dataValues.new_obd_firmware_version_id,
            };
            await dbHandler.update('device', { where: { id: deviceId } }, updatedDevice);
            logger.info(`Table 'device' updated for device ${deviceId}`);

            // update update_start_time in 'obd_firmware_update' to current time
            const updatedFirmwareUpdate = {
                update_start_time: currentTime,
            };
            await dbHandler.update('obd_firmware_update', { where: { device_id: deviceId } }, updatedFirmwareUpdate);
            logger.info(`Table 'obd_firmware_update' updated for device ${deviceId}`);

            // 3) Respond file to the client
            responder.send(res, true, '<SuccessfulFirmwareDownload>', null, file); // send the whole response object to the client
            // res.send(file); // would only send the file to the client
            // res.end(file); // makes e. g. a pdf actually viewable in a browser
        })
        .catch(error => responder.falseAndSend(res, error, 500));
}

exports.downloadFirmwareUpdate = downloadFirmwareUpdate;

// ######## HELPER FUNCTIONS ########
/**
 * Helper function to check for a config update for a provided device.
 *
 * @param {string} deviceId the id of the device
 *
 * @returns {Promise}
 *
 * @see checkForUpdates()
 */
function checkForConfigUpdate(deviceId) {
    const dbHandler = new DbHandler();

    // query condition
    const condition = {
        where: { device_id: deviceId },
    };

    // get the entry for the provided device_id in the config_update table
    return dbHandler.findOne('obd_config_update', condition)
        .then(deviceConfigUpdate => new Promise((resolve, reject) => {
            logger.verbose(`Sequelize findOne('obd_config_update') response: ${deviceConfigUpdate}`);

            // check if an entry actually exists for that device already
            if (!deviceConfigUpdate) {
                // if no device exists in that table, a new one is created that actually needs a config update with the standard config
                logger.info(`No device has been found for the provided Id ${deviceId}`);
                logger.info('A new entry shall be created here');

                // the new entry for the device pointing out the default config determined in the config.js should be downloaded
                const newConfigUpdate = {
                    device_id: deviceId,
                    update_needed: true,
                    update_start_time: '1980-01-01', // 01.01.1980 till the first update actually occurrs
                    url: config.urlDefaultConfig,
                    check_sum: config.checksumDefaultConfig,
                };

                // insert the new config update in the obd_config_update table
                dbHandler.create('obd_config_update', newConfigUpdate)
                    .then((createdConfigUpdate) => {
                        logger.info(`A new config_update entry has been created for device: ${createdConfigUpdate.device_id}`);
                        resolve(createdConfigUpdate.dataValues);
                    })
                    .catch(error => reject(error));
            } else {
                // if the device existed already in that table, simply return if that device actually needs a config update
                logger.info(`A device has been found for the provided Id ${deviceId}`);
                resolve(deviceConfigUpdate.dataValues);
            }
        }))
        // handle the result of obd_config_update
        .then(configUpdate => new Promise((resolve, reject) => {
            // check if the device actually needs a config update
            if (configUpdate.update_needed) {
                logger.info(`Config update needed for device ${deviceId}`);
                // 1a) If so, return a boolean determining that as well as the download Url and the checksum.
                resolve((({
                    device_id, update_needed, url, check_sum,
                }) => ({
                    device_id, update_needed, url, check_sum,
                }))(configUpdate));
            } else {
                logger.info(`No config update needed for device ${deviceId}`);
                // 1b) If not, simply return a false flag for the config update.
                resolve((({ device_id, update_needed }) => ({ device_id, update_needed }))(configUpdate));
            }
        }))
        .then((configUpdateResult => Promise.resolve(configUpdateResult)))
        .catch(error => Promise.reject(error));
}

/**
 * Helper function to check for a firmware update for a provided device.
 *
 * @param {string} deviceId the id of the device

 * @returns {Promise}
 *
 * @see checkForUpdates()
 */
function checkForFirmwareUpdate(deviceId) {
    const dbHandler = new DbHandler();

    // 2) Next, it is checked if the provided device needs a firmware update.
    logger.info(`Check for firmware updates for device ${deviceId}`);

    // 2.1) First, check if that device already has an entry in obd_firmware_update.
    // query condition
    const firmwareUpdateCondition = {
        where: { device_id: deviceId },
    };

    // get the device entry of obd_firmware_update
    return dbHandler.findOne('obd_firmware_update', firmwareUpdateCondition)
        .then(deviceFirmwareUpdate => new Promise((resolve, reject) => {
            // check if obd_firmware_update has an entry for the provided device
            if (deviceFirmwareUpdate) {
                // 2.1a) If an entry already exist, simply persume with 2.2)
                logger.info(`A firmware_update entry already exists for device ${deviceId}`);
                resolve(deviceFirmwareUpdate.dataValues);
            } else {
                // 2.1b) If no entry exists yet, create a default entry that leads to a firmware update to the default firmware
                logger.info(`A firmware_update entry does not yet exist for device ${deviceId}`);

                // the new entry for the device pointing out the default firmware determined in the config.js should be downloaded
                const newFirmwareUpdate = {
                    device_id: deviceId,
                    new_obd_firmware_version_id: config.defaultFirmwareVersionId,
                    update_start_time: '1980-01-01', // it stays 01.01.1980 till the first update actually occurrs
                };

                // insert the new firmware update in the obd_firmware_update table
                dbHandler.create('obd_firmware_update', newFirmwareUpdate)
                    .then((createdFirmwareUpdate) => {
                        logger.info(`A new firmware_update entry has been created for device: ${createdFirmwareUpdate.device_id}`);
                        resolve(createdFirmwareUpdate.dataValues);
                    })
                    .catch(error => reject(error));
            }
        }))
        .then((firmwareUpdate => new Promise((resolve, reject) => {
            // 2.2) Second, get the actual firmware version of that device out of the 'device' table.
            logger.info(`Get current firmware version for device ${deviceId}`);

            // query condition
            const condition = {
                where: { id: deviceId },
            };

            // get the current firmware version of the device
            dbHandler.findOne('device', condition)
                .then((requestedDevice) => {
                    logger.info(`Current Firmware version for device ${requestedDevice.id} is ${requestedDevice.current_obd_firmware_version_id}`);
                    // add the current firmware version of the device to the object to be resolved
                    firmwareUpdate.currentFirmwareId = requestedDevice.current_obd_firmware_version_id;
                    resolve(firmwareUpdate);
                })
                .catch(error => reject(error));
        })))
        // check if the device needs a firmware update
        .then(firmwareUpdate => new Promise((resolve, reject) => {
            // 2.3) Third, check if that firmware version is smaller than the newest firmware version
            // of table obd_firmware_update (column new_obd_firmware_version_id).
            logger.info(`Check if device ${deviceId} needs a firmware update`);

            // modify the firmware response (e.g. delete data that should not be returned to client)
            delete firmwareUpdate.update_start_time;

            // check if an update is required
            if (firmwareUpdate.currentFirmwareId < firmwareUpdate.new_obd_firmware_version_id) {
                // 2.4a) If the device needs a firmware update, return a boolean determining that as well as the download Url
                // and the checksum.
                logger.info(`Device ${deviceId} requires a firmware update`);

                // extract the checksum of that firmware file from the database
                logger.info('Extracting checksum of firmware file to be downloaded');

                // query condition
                const condition = {
                    where: { id: firmwareUpdate.new_obd_firmware_version_id },
                };

                // modify the firmware response (e.g. delete data that should not be returned to client)
                delete firmwareUpdate.currentFirmwareId;
                delete firmwareUpdate.new_obd_firmware_version_id;
                firmwareUpdate.update_needed = true;

                // get the checksum and url of the firmware file to be downloaded
                dbHandler.findOne('obd_firmware_version', condition)
                    .then((version) => {
                        logger.info(`Checksum for device ${deviceId} and firmware ${version.id} is ${version.check_sum}`);
                        // add additional data to the final response
                        firmwareUpdate.check_sum = version.check_sum;
                        firmwareUpdate.url = version.url + version.filename;

                        // resolve the final response
                        resolve(firmwareUpdate);
                    })
                    .catch(error => reject(error));
            } else {
                // 2.4b) If not, simply return a false flag for the firmware update.
                logger.info(`Device ${deviceId} does not need a firmware update`);

                // modify the firmware response (e.g. delete data that should not be returned to client)
                firmwareUpdate.update_needed = false;
                delete firmwareUpdate.currentFirmwareId;
                delete firmwareUpdate.new_obd_firmware_version_id;

                // resolve the final response
                resolve(firmwareUpdate);
            }
        }))
        .then(firmwareUpdate => Promise.resolve(firmwareUpdate))
        .catch(error => Promise.reject(error));
}
