info:
  title: OBD-Data-Upload
  version: 0.0.9
  description: "This page contains the Swagger description of the OBD-Data-Upload API"
swagger: '2.0'
basePath: /api
tags:
- name: "Heartbeats"
  description: "Everything regarding heartbeats"
- name: "Updates"
  description: "Everything regarding updates"
- name: "Recfiles"
  description: "Uploading recfiles"
paths:
  /heartbeats:
    post:
      tags:
      - "Heartbeats"
      summary: "Writes a heartbeat into the WIFI log"
      operationId: "createHeartbeat"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Heartbeat that needs to be inserted into the WIFI log. Note, that the combination of device_id and time needs to be unique"
        required: true
        schema:
          $ref: "#/definitions/Heartbeat"
      responses:
        200:
          description: "Successful operation"
          schema:
            $ref: "#/definitions/HeartbeatResponse"
        422:
          description: "Invalid input"
          schema:
            $ref: "#/definitions/InvalidInput"
        500:
          description: "Internal server errors"
          schema:
            $ref: "#/definitions/ServerError"

  /updates/check:
    get:
      tags:
      - "Updates"
      summary: "Check for firmware and config updates"
      operationId: "checkForUpdates"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
        - in: "query"
          name: "device_id"
          type: string
          description: "Contains the Id of the device that should be checked for updates"
          required: true
      responses:
        200:
          description: "Successful operation"
          schema:
            $ref: "#/definitions/UpdateCheckResponse"
        422:
          description: "Invalid input"
          schema:
            $ref: "#/definitions/InvalidInput"
        500:
          description: "Internal server errors"
          schema:
            $ref: "#/definitions/ServerError"
  /updates/firmware:
    get:
      tags:
      - "Updates"
      summary: "Download a firmware update"
      operationId: "downloadFirmwareUpdate"
      consumes:
      - "application/x-www-form-urlencoded"
      produces:
      - "application/json"
      parameters:
      - in: "query"
        name: "device_id"
        type: string
        required: true
        description: "The Id of the device"
      - in: "query"
        name: "new_firmware_url"
        type: string
        format: uri
        required: true
        description: "The Url of the firmware to be downloaded"
      # - in: "query"
      #   name: "new_firmware_filename"
      #   type: string
      #   required: true
      #   description: "The name of the firmware file"
      responses:
        200:
          description: "Successful operation"
          schema:
            $ref: "#/definitions/DownloadFirmwareResponse"
        422:
          description: "Invalid input"
          schema:
            $ref: "#/definitions/InvalidInput"
        500:
          description: "Internal server errors"
          schema:
            $ref: "#/definitions/ServerError"
  /updates/config:
    get:
      tags:
      - "Updates"
      summary: "Download a config update"
      operationId: "downloadConfigUpdate"
      consumes:
      - "application/x-www-form-urlencoded"
      produces:
      - "application/json"
      parameters:
      - in: "query"
        name: "device_id"
        type: string
        required: true
        description: "The Id of the device"
      - in: "query"
        name: "new_config_url"
        type: string
        format: uri
        required: true
        description: "The Url of the config to be downloaded"
      # - in: "query"
      #   name: "new_config_filename"
      #   type: string
      #   required: true
      #   description: "The name of the config file"
      
      # TODO: Evaluate, if the new_config_filename query param is actually needed
      responses:
        200:
          description: "Successful operation"
          schema:
            $ref: "#/definitions/DownloadConfigResponse"
        422:
          description: "Invalid input"
          schema:
            $ref: "#/definitions/InvalidInput"
        500:
          description: "Internal server errors"
          schema:
            $ref: "#/definitions/ServerError"

  /recfiles:
    post:
      tags:
      - "Recfiles"
      summary: "Upload a recfile to be moved to the recfile queue"
      description: |
        Uploading one or a batch of recfiles. Note, that uploading a batch of Recfiles with limited size via one request is possible.
        Therefore, the first file needs to be named 'recfile' and the checksum 'sha'. Each subsequent recfile must be named 'recfileX' while 'X' represents a number. 
        Furthermore, the related checksum must be named 'shaX' while both 'X' represent the same number. 
        
        
        Example: recfile87 and sha87.
      operationId: "uploadRecfile"
      security:
        - AuthToken: []
      consumes:
      - "multipart/form-data" 
      produces:
      - "application/json"
      parameters:
      - name: "vehicle_device_token"
        in: "header"
        description: "An authorization token"
        type: string
        required: true
      - in: "formData"
        name: "device_id"
        type: string
        description: "The Id of the device"
        required: true
      - in: "formData"
        name: "user_vehicle_id"
        type: integer
        description: "The id of the user_vehicle"
        required: true
      - in: "formData"
        name: "sha"
        type: string
        description: "The sha1 checksum of the first recfile"
        required: true
      - name: "recfile"
        in: "formData"
        description: "The first recfile to be uploaded"
        required: true
        type: file
      - in: "formData"
        name: "longitude"
        type: number
        description: "The longitude"
      - in: "formData"
        name: "latitude"
        type: number
        description: "The latitude"
      responses:
        200:
          description: "Successful operation"
          schema:
            $ref: "#/definitions/UploadRecfileResponse"
        413:
          description: "Payload too large"
          schema:
            $ref: "#/definitions/PayloadTooLarge"
        422:
          description: "Invalid input"
          schema:
            $ref: "#/definitions/InvalidInput"
        500:
          description: "Internal server errors"
          schema:
            $ref: "#/definitions/ServerError"

definitions:
  BasicSuccess:
    type: object
    properties:
      success:
        description: "Flag to determine if operation was successful"
        type: boolean
        example: true
      status:
        description: "The HTTP status code"
        type: number
        example: 200

  InvalidInput:
    type: object
    properties:
      success:
        description: "Flag to determine if operation was successful"
        type: boolean
        example: false
      message:
        description: "An error tag. For more information check the log file"
        type: string
        example: "<No device_id or not parseable to string>"
      status:
        description: "The HTTP status code"
        type: number
        example: 422

  PayloadTooLarge:
    type: object
    properties:
      success:
        description: "Flag to determine if operation was successful"
        type: boolean
        example: false
      message:
        description: "An error tag. For more information check the log file"
        type: string
        example: "<TooManyRecfiles>"
      status:
        description: "The HTTP status code"
        type: number
        example: 413

  ServerError:
    type: object
    properties:
      success:
        description: "Flag to determine if operation was successful"
        type: boolean
        example: false
      message:
        description: "An error tag. For more information check the log file"
        type: string
        example: "<Create_obd_config_update>"
      status:
        description: "The HTTP status code"
        type: number
        example: 500

  Heartbeat:
    type: object
    properties:
      device_id:
        description: "The Id of the device"
        type: string
        example: "123abc"
      time: 
        description: "Current timestamp"
        type: string
        format: date-time
        example: "2021-01-01 00:00:00"
      time_previous_wifi_lost: 
        description: "Time when the previous Wifi connection was lost"
        type: string
        format: date-time
        example: "2013-05-01 00:00:00"
      time_max_rssi: 
        description: "Time when the max RSSI was hit"
        type: string
        format: date-time
        example: "2011-01-01 00:00:00"
      ssid_current_wifi: 
        description: "The SSID of the current Wifi"
        type: string
        example: "asdasdasdhwadjhasdja"
      lat_current_wifi_con: 
        description: "The Lat of the current Wifi connection"
        type: number
        example: 15.123
      long_current_wifi_con: 
        description: "The Long of the current Wifi connection"
        type: number
        example: 250
      ssid_previous_wifi: 
        description: "The SSID of the previous Wifi"
        type: string
        example: "daksdjawqdasd123"
      rssi_previous_wifi_lost: 
        description: "The RSSI of when the previous Wifi was lost"
        type: number
        example: 21
      lat_previous_wifi_lost: 
        description: "The Lat of when the previous Wifi was lost"
        type: number
        example: 12
      long_previous_wifi_lost: 
        description: "The Long of when the previous Wifi was lost"
        type: number
        example: 14
      max_rssi: 
        description: "The maximum RSSI"
        type: number
        example: 20
      lat_max_rssi: 
        description: "The Lat of the maximum RSSI"
        type: number
        example: 17
      long_max_rssi: 
        description: "The Long of the maximum RSSI"
        type: number
        example: 14
      free_disk_space: 
        description: "The amount of free disk space"
        type: number
        example: 4444 
      number_of_local_files: 
        description: "The amount of local files"
        type: integer
        example: 1234            
    required:
      - device_id
      - time
      - time_previous_wifi_lost
      - time_max_rssi

  HeartbeatResponse:
    type: object
    allOf:
      - $ref: "#/definitions/BasicSuccess"
      - properties:
          message:
            description: "A success tag. For more information check the log file"
            type: string
            example: "<SuccessfulInsert>"
          data:
            description: "The actual response data"
            type: object
            allOf:
            - $ref: "#/definitions/Heartbeat"
  
  UpdateCheckResponse:
    type: object
    allOf:
      - $ref: "#/definitions/BasicSuccess"
      - properties:
          message:
            description: "A success tag. For more information check the log file"
            type: string
            example: "<SuccessfulUpdateCheck>"
          data:
            description: "The actual response data"
            type: object
            properties:
              config:
                description: "The response of the check for a config update"
                type: object
                properties:
                  device_id:
                    description: "The Id of the device"
                    type: string
                    example: "123abc"
                  update_needed:
                    description: "Flag to determine if the device needs a config update"
                    type: boolean
                    example: true
                  url:
                    description: "The Url of the new config to be downloaded"
                    type: string
                    example: "https://www.default-config.de/test.conf"
                  check_sum:
                    description: "The checksum of the config file"
                    type: string
                    example: "DASDJSDFKSDAJ332423"
              firmware:
                description: "The response of the check for a firmware update"
                type: object
                properties:
                  device_id:
                    description: "The Id of the device"
                    type: string
                    example: "123abc"
                  update_needed:
                    description: "Flag to determine if the device needs a config update"
                    type: boolean
                    example: true
                  url:
                    description: "The Url of the new firmware to be downloaded"
                    type: string
                    example: "https://www.default-firmware.de/test.firmv"
                  # filename:
                  #   description: "The name of the firmware file"
                  #   type: string
                  #   example: "FirmwareV7"
                  check_sum:
                    description: "The checksum of the firmware file"
                    type: string
                    example: "DASDJSDFKSDAJ332423"

  UploadRecfileResponse: 
    type: object
    allOf:
      - $ref: "#/definitions/BasicSuccess"
      - properties:
          message:
            description: "A success tag. For more information check the log file"
            type: string
            example: "<SuccessfulRecfileUpload>"
          data:
            type: array
            description: "Contains possible errors while moving recfiles to the queue. 
              Note, that this means, that all recfiles without errors were still successfully
              processed"
            example: []

  DownloadFirmwareResponse:
    type: object
    allOf:
      - $ref: "#/definitions/BasicSuccess"
      - properties:
          message:
            description: "A success tag. For more information check the log file"
            type: string
            example: "<SuccessfulFirmwareDownload>"
          data:
            description: "The firmware file"
            type: string
            format: file

  DownloadConfigResponse:
    type: object
    allOf:
      - $ref: "#/definitions/BasicSuccess"
      - properties:
          message:
            description: "A success tag. For more information check the log file"
            type: string
            example: "<SuccessfulConfigDownload>"
          data:
            description: "The config file"
            type: string
            format: file