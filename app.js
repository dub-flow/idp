// module dependencies
const createError = require('http-errors');
const express = require('express');
const expressValidator = require('express-validator');
const rateLimit = require('express-rate-limit');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const helmet = require('helmet');
const fs = require('fs');
const csurf = require('csurf');
const cookieParser = require('cookie-parser')

// custom modules
const config = require('./config');
const database = require('./src/database');
const logger = require('./logger.js');

// main router
const mainRouter = require('./src/routes/');

// swagger dependencies
const swaggerDoc = YAML.load('./swagger-doc.yaml');

// starting express
const app = express();

// parse cookies
// we need this because "cookie" is true in csrfProtection
app.use(cookieParser())

// CSRF protection
const csrfMiddleware = csurf({
    cookie: true
});

// define rate limit e.g. for mitigating DDoS attacks
const limiter = rateLimit({
    windowMs: 60 * 1000, // 1 minute
    max: 100, // max number of connections during windowMs
});

// helmet configuration for activating HTTP security headers
app.use(
    helmet(),
    // sets X-Frame-Options header to prevent the website from getting framed (clickjacking)
    helmet.frameguard({ action: 'deny' }),
    // sets X-Content-Type-Options header to prevent loading of styles and scripts if they have an incorrect MIME type
    helmet.noSniff(),
    // sets the X-XSS-Protection header to activate the browser intern cross-site scripting filters
    helmet.xssFilter(),
    // sets X-Download-Options header to prevent old IE versions from downloading page contexts
    helmet.ieNoOpen(),
    // hide X-Powered-By header in order to avoid information disclosure
    helmet.hidePoweredBy(),
    // prevent browser from DNS prefetching
    helmet.dnsPrefetchControl(),
);

// parse incoming request bodys as json to handle POST parameters (x-www-form-urlencoded)
app.use(express.json());
app.use(express.urlencoded({
    // allows posting of nested objects
    extended: true,
}));

// parse incoming requests containing files
app.use(fileUpload());

// validate incoming request parameters
app.use(expressValidator());

// apply CSRF middleware to the app
app.use(csrfMiddleware);

// apply limiter to the app
app.use(limiter);

// connect the routers to the app
app.use(config.webserver.baseUrl, mainRouter);

// swagger route for API documentation
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));

// log HTTP request details
app.use(morgan('dev'));

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'dev' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

// create folder for the recfile queue if it does not yet exist
fs.existsSync(config.recfileQueueFolder) || fs.mkdirSync(config.recfileQueueFolder);
fs.existsSync(config.recfileErrorQueueFolder) || fs.mkdirSync(config.recfileErrorQueueFolder);

logger.info('Folders for recfile handling exist');

// this shall print a warning since connection timeouts are not handled too well by Sequelize.
// Thus, debugging a non-establishable database connection can be detected easier
logger.warn('Try establishing a connection to the database... ');
// testing connection to database
database.authenticate()
    .then(() => {
        // if authenticated has been successful
        logger.info('... Successfully connected to database');

        app.emit('started');
    })
    .catch((err) => {
        logger.error(`Unable to connect to the database due to error: ${err}`);
    });

module.exports = app;
